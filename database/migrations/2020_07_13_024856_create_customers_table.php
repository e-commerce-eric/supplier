<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('first_name', 100)->nullable();
			$table->string('last_name', 100)->nullable();
			$table->string('address_1', 100)->nullable();
			$table->string('address_2', 100)->nullable();
			$table->string('town_or_city', 100)->nullable();
			$table->string('county', 100)->nullable();
			$table->string('postalcode', 100)->nullable();
			$table->string('country', 100)->nullable();
			$table->string('phone', 100)->nullable();
			$table->string('ship_city__or_town', 100)->nullable();
			$table->string('ship_county', 100)->nullable();
			$table->string('ship_address', 100)->nullable();
			$table->string('ship_country', 100)->nullable();
			$table->string('email', 191)->unique();
			$table->boolean('activated')->default(0);
			$table->string('email_token', 191)->nullable();
			$table->string('password', 191)->nullable();
			$table->string('ip_address', 50)->nullable();
			$table->dateTime('email_verified_at')->nullable();
			$table->string('remember_token', 191)->nullable();
			$table->string('last_edited_by')->nullable();
			$table->string('reference_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}

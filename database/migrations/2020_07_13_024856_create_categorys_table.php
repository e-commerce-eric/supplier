<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategorysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categorys', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('category_name', 50)->nullable();
			$table->string('description', 50)->nullable();
			$table->string('picture', 50)->nullable();
			$table->boolean('need_size')->nullable();
			$table->boolean('need_color')->nullable();
			$table->integer('status')->default(1)->index('status');
			$table->string('last_edited_by')->nullable();
			$table->string('reference_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categorys');
	}

}

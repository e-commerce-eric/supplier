<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_details', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('product_id')->index('product_id');
			$table->bigInteger('order_id')->nullable()->index('order_id');
			$table->string('order_number', 191)->nullable();
			$table->decimal('price', 10, 2)->nullable();
			$table->string('quantity', 191)->nullable();
			$table->decimal('discount', 10,2)->nullable();
			$table->decimal('total', 10,2)->nullable();
			$table->string('size', 191)->nullable();
			$table->string('color', 191)->nullable();
			$table->string('dispatch_date', 191)->nullable();
			$table->string('delivery_date', 191)->nullable();
			$table->string('last_edited_by')->nullable();
			$table->string('reference_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_details');
	}

}

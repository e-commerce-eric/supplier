<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrderSettlementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('order_settlements', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('order_id')->nullable()->index('order_id');
			$table->bigInteger('supplier_id')->unsigned()->index('supplier_id');
			$table->string('settlement_date', 25)->nullable();
			$table->string('amount', 25)->nullable();
			$table->string('payment_reference_number', 191)->nullable();
			$table->string('last_edited_by');
			$table->string('reference_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('order_settlements');
	}

}

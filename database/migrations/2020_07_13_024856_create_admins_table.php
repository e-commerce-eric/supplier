<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admins', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('company_name', 100)->nullable();
			$table->string('employee_number', 100)->nullable();
			$table->string('reference', 100)->nullable();
			$table->string('last_updated_by', 100)->nullable();
			$table->string('email', 191)->unique();
			$table->boolean('activated')->default(0);
			$table->string('email_token', 191)->nullable();
			$table->string('password', 191)->nullable();
			$table->string('ip_address', 50)->nullable();
			$table->dateTime('email_verified_at')->nullable();
			$table->string('remember_token', 191)->nullable();
			$table->string('last_edited_by')->nullable();
			$table->string('reference_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin');
	}

}

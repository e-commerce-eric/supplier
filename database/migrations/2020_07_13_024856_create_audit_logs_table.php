<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuditLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('audit_logs', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('reference_id')->nullable();
			$table->string('client_ip')->nullable();
			$table->string('operation_name')->nullable();
			$table->string('last_edited_by')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('audit_logs');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('supplier_id')->unsigned()->index('supplier_id');
			$table->bigInteger('category_id')->index('category_id');			
			$table->string('product_name', 191, 191)->nullable();
			$table->decimal('previous_price', 10, 3)->nullable();
            $table->string('days_required_to_prepare', 191)->nullable();
            $table->boolean('dates')->nullable();	
			$table->string('capacity_for_service', 191)->nullable();
			$table->string('product_description', 2000)->nullable();
			$table->string('quantity_per_unit', 191)->nullable();			
			$table->decimal('unit_price', 10, 3)->nullable();
			$table->decimal('total_product_price', 10, 3)->nullable();			
			$table->text('available_size', 1000)->nullable();
			$table->text('available_color', 1000)->nullable();
			$table->boolean('approved')->default(0);
			$table->boolean('available')->default(1);
			$table->decimal('discount', 10,3)->nullable();
			$table->string('unit_weight', 191)->nullable();
			$table->string('units_on_order', 191)->nullable();
			$table->string('units_on_stock', 191)->nullable();
			$table->string('current_order', 191)->nullable();
			$table->text('note', 1000)->nullable();
			$table->string('product_reference_number', 191)->nullable();
			$table->string('last_edited_by');
			$table->string('reference_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}

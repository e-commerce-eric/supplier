<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('order_number', 191)->nullable();
			$table->bigInteger('customer_id')->unsigned()->index('customer_id');
			$table->bigInteger('payment_id')->nullable()->index('payment_id');
			$table->bigInteger('shipper_id')->nullable()->index('shipper_id');
			//foreignkey--->of product_id
			$table->string('ship_date', 25)->nullable();
			$table->string('required_date', 25)->nullable();
			$table->tinyInteger('transaction_status')->nullable();//2 for delivered//1 for dispatched//3 for paid and pending delivery//-4not paid and not delivered//-1 returned
			$table->string('payment_date', 25)->nullable();
			$table->boolean('paid')->default(0);
			$table->string('error_msg', 191)->nullable();
			$table->string('last_edited_by');
			$table->string('reference_id');
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
            $table->foreign('shipper_id', 'orders_ibfk_1')->references('id')->on('shippers')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('customer_id', 'orders_ibfk_2')->references('id')->on('customers')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('payment_id', 'orders_ibfk_3')->references('id')->on('payments')->onUpdate('CASCADE')->onDelete('CASCADE');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropForeign('orders_ibfk_1');
			$table->dropForeign('orders_ibfk_2');
			$table->dropForeign('orders_ibfk_3');

		});
	}

}

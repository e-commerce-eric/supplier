<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('suppliers', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('company_name', 100)->nullable();
			$table->string('first_name', 100)->nullable();
			$table->string('last_name', 100)->nullable();			
			$table->string('address_1', 100)->nullable();
			$table->string('address_2', 100)->nullable();
			$table->string('town_or_city', 100)->nullable();
			$table->string('county', 100)->nullable();
			$table->string('postalcode', 100)->nullable();
			$table->string('country', 100)->nullable();
			$table->string('phone', 100)->nullable();
			$table->string('url', 100)->nullable();
			$table->string('type_of_goods_or_service', 100)->nullable();
			$table->string('kra', 100)->nullable();
			$table->string('registration_cert', 100)->nullable();
			$table->string('license', 100)->nullable();
			// $table->string('other_documents', 100)->nullable();
			$table->string('logo', 100)->nullable();
			$table->string('payments_methods', 100)->nullable();
			$table->text('notes', 500)->nullable();
			$table->string('email', 191)->unique();
			$table->boolean('activated')->default(0);
			$table->boolean('approved')->default(0);
			$table->string('email_token', 191)->nullable();
			$table->string('password', 191)->nullable();
			$table->string('ip_address', 50)->nullable();
			$table->dateTime('email_verified_at')->nullable();
			$table->tinyInteger('user_type')->nullable();//1 for admin, 2 for seller, 3 for buyer, 4 for shipper
			$table->string('remember_token', 191)->nullable();
			$table->string('last_edited_by')->nullable();
			$table->string('reference_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('suppliers');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDropPointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('drop_points', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->string('location', 191)->nullable();
			$table->decimal('price', 10,3)->nullable();
			$table->boolean('status')->nullable();
			$table->string('last_edited_by')->nullable();
			$table->string('reference_id',191)->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('drop_points');
	}

}

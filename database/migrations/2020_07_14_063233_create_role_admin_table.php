<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoleAdminTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('role_admin', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('role_id')->unsigned()->index('role_admin_role_id_foreign');
			$table->bigInteger('admin_id')->unsigned()->index('role_admin_admin_id_foreign');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('role_admin');
	}

}

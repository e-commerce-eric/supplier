<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rates', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('category_id')->index('category_id');
			$table->decimal('profit', 10,3)->nullable();
			$table->decimal('VAT', 10,3)->nullable();
			$table->boolean('type')->nullable();
			$table->string('last_edited_by')->nullable();
			$table->string('reference_id')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rates');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOrderSettlementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_settlements', function(Blueprint $table)
		{
            $table->foreign('supplier_id', 'order_settlements_ibfk_1')->references('id')->on('suppliers')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('order_id', 'order_settlements_ibfk_2')->references('id')->on('orders')->onUpdate('CASCADE')->onDelete('CASCADE');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_settlements', function(Blueprint $table)
		{
			$table->dropForeign('order_settlements_ibfk_1');
			$table->dropForeign('order_settlements_ibfk_2');
		});
	}

}

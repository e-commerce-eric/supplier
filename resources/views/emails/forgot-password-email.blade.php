<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css" rel="stylesheet" />
   
    <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
   
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
</head>
<body>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                <p>Hi {{$name}},</p><br>
                You recently requested to reset your password for your seller account.<br>
                <div> Kindly click the <a href="http://localhost:3000/reset-password/?email_token={{$email_token}}"> link  
                          </a>to reset it.
                    </div><br>
                    <div>If you did not request a password request, please ignore this email.</div><br>
                    If you are having trouble clicking the link, copy and past the url below on your browser.
                    <!-- <div>store front url</div> -->
                    

                    <div> http://localhost:3000/reset-password/?email_token={{$email_token}}</div>
                    <br> Kind Regards          

                </div>
            </div>
        </div>
    </div>
</body>
</html>
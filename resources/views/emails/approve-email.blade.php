<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', '') }}</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css" rel="stylesheet" />
   
    <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
   
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
</head>
<body>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                <p>Hi Admin,</p>
                Kindly note there is a new supplier pending approval

                <div>Kindly click this <a href="{{url('/approve-supplier/?token='.$email_token)}}"> link </a> to login and approve.</div> <br>  
                    If you are having trouble clicking the link, copy and past the url below on your browser.<br>
                    <!-- <div>store front url</div> -->
                    <div>{!! url('/approve-supplier/?token='.$email_token) !!} into your web browser</p></div>
                    <br> Kind Regards              

                </div>
            </div>
        </div>
    </div>
</body>
</html>


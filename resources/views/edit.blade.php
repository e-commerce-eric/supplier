<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Laravel Uploading</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Fonts -->

<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<!-- Styles -->

<style>

.container {

margin-top:2%;

}

</style>

</head>

<body>


<div class="container">

<div class="row">


<div class="col-md-8"><h2>Laravel Multiple File Uploading With Bootstrap Form</h2>

</div>

</div>

<br>

<div class="row">

<div class="col-md-3"></div>

<div class="col-md-6">
<form action="{{ route("product.update", [$product->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
                  
            <div class="product">
                <label for="product">Product</label>

                    @foreach($product as $product => $product_id)
                    <label for="product">{{ $product_id['service_name'] }}</label>
                    <input type="text" name="name" class="form-control"  placeholder="Product Name" >
                    <input type="file" class="form-control" name="photos[]" multiple />                        
                    @endforeach
               
            </div>
            <div class="form-group {{ $errors->has('url') ? 'has-error' : '' }}">
              
            <div>
                <input class="btn btn-danger" type="submit" value="save">
            </div>
        </form>
</div>

</div>

</div>

</body>

</html>
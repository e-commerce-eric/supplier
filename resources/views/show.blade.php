<!doctype html>

<html lang="{{ app()->getLocale() }}">

<head>

<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Laravel Uploading</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Fonts -->

<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

<!-- Styles -->

<style>

.container {

margin-top:2%;

}

</style>

</head>



<div class="card">
    <div class="card-header">
    Callback List
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
            @foreach($product as $product)
                <tr>
                    <th>
                        File Name
                    </th>
                    <td>
                    {{ $product->product_name }}
                    </td>
                </tr>
                <tr>
                    <th>
                        File Url
                    </th>
                    <td>
                        {{ $product->file_path }}
                    </td>
                </tr>
               @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>

</html>
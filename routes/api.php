<?php

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
    Route::post('register', 'V1\SuppliersController@register');
	Route::post('login', 'V1\SuppliersController@login');
	Route::get('logout', 'V1\SuppliersController@logout');
	Route::get('supplier', 'V1\SuppliersController@getAuthenticatedSupplier');
	Route::post('refresh', 'V1\SuppliersController@refresh');
	Route::get('verify-email/{token}', 'V1\SuppliersController@emailVerification');
//forgot password
	Route::post('forgot-password', 'V1\SuppliersController@forgotPassword');
	Route::post('update-password-forgot-password', 'V1\SuppliersController@forgotPasswordUpdate');
//reset password
	Route::post('password-reset', 'V1\SuppliersController@resetPassword');
	Route::post('reset-password', 'V1\SuppliersController@update');


	//getauthenticated supplier
	// Route::get('autheniticated-supplier', 'V1\SuppliersController@getAuthenticatedSupplier');


	Route::get('category', 'V1\CategoryController@getAllCategorys');
	Route::get('category/{id}', 'V1\CategoryController@getCategory');
	Route::post('category', 'V1\CategoryController@createCategory');
	Route::put('category/{id}', 'V1\CategoryController@updateCategory');
	Route::delete('category/{id}','V1\CategoryController@deleteCategory');
	
	Route::get('products', 'V1\ProductsController@getAllProducts');
	Route::get('products/{id}', 'V1\ProductsController@getProduct');
	Route::post('products', 'V1\ProductsController@createProduct');
	Route::post('products/{id}', 'V1\ProductsController@updateProduct');
	Route::delete('products/{id}','V1\ProductsController@deleteProduct');
	Route::get('supplier-products', 'V1\ProductsController@getProductsForOneSupplier');


	Route::get('sales-report', 'V1\SupplierReportsController@salesReport');
	Route::get('delivery-report', 'V1\SupplierReportsController@deliveryReport');
	Route::get('settlement-report', 'V1\SupplierReportsController@settlementReport');
	Route::get('returned-items-report', 'V1\SupplierReportsController@returnedItemsReport');



	








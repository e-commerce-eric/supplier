<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
    'supplier_id',
    'category_id',
    'product_name',
    'product_description',
    'quantity_per_unit',
    // 'price_for_all',
    'unit_price',
    'available_size',
    'size',
    'color',
    'available_color',
    'available',
    'discount',
    'unit_weight',
    'units_on_order',
    'units_on_stock',
    'current_order',
    'note',
    'product_reference_number',
    'last_edited_by',
    'reference_id',
    'approved'
    ];

    public function image()
    {
        return $this->hasMany(Image::class, 'product_id');
    }

    public function firstImage()
    {
        return $this->hasOne(Image::class);
    }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
    public function orderdetails()
    {
        return $this->belongsTo('App\Models\OrderDetails');
    }
}

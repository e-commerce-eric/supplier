<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $fillable = [
        'category_id', 
        'profit', 
        'type',
        'last_edited_by',
		'reference_id'
    ];
}

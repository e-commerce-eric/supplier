<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
    'product_id',
    'file_path',
    'last_edited_by',
    'reference_id'
    ];
    public function product()
{
    return $this->belongsTo(Product::class, 'product_id');
}
}

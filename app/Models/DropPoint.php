<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DropPoint extends Model
{
    protected $fillable = [
        'location', 
        'price', 
        'status',
        'last_edited_by',
		'reference_id'
    ];
}

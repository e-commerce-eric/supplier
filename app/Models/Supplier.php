<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Supplier extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_name',    
        'first_name',
        'last_name',
        'user_type',
        'address_1',
        'address_2' ,
        'town_or_city' ,
        'county',
        'postalcode',
        'country',
        'phone' ,
        'url',
        'type_of_goods_or_service',
        'kra',
        'registration_cert',
        'license',
        // 'other_documents',
        'logo' ,
        'payments_methods',
        'notes',
        'email' ,
        'email_token' ,
        'activated',
        'password',
        'ip_address',
        'last_edited_by',
        'reference_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function product()
    {
        return $this->hasMany('App\Models\Product');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categorys";
    protected $fillable = [
        'picture',
        'category_name',
        'description',
        'status',  
        'last_edited_by',
		'reference_id'
    ];
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}

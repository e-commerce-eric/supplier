<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    protected $table = "order_details";
    public function products()
    {
        return $this->belongsTo('App\Models\Product');
    }
}

<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\SendPasswordResetEmail;
use Mail;

class SendPasswordResetLinkSeller implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $email;
    protected $email_token;
    protected $name;

    /**
    * The number of times the job may be attempted.
    *
    * @var int
    */
   public $tries = 5;

   /**
    * The number of seconds the job can run before timing out.
    *
    * @var int
    */
   public $timeout = 120;
   
   /**
    * Create a new job instance.
    *
    * @return void
    */
   public function __construct($email, $email_token, $name)
   {
       $this->email = $email;
       $this->email_token = $email_token;
       $this->name = $name;
   }

   /**
    * Execute the job.
    *
    * @return void
    */
   public function handle()
   {
       try 
       {

            $emailto = new SendPasswordResetEmail($this->email_token,$this->name);
            Mail::to($this->email)->send($emailto);
      
       } catch (\Exception $e) 
       {
            $Exception_Error_Message = $e->getMessage();
           \Log::info("Could Not Execute Send Email Verification Job! Exception Message: $Exception_Error_Message");   
       }
      
   }
}
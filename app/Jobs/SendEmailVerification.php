<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Mail\SendPasswordResetEmail;
use App\Mail\EmailVerification;
use App\Mail\SendForgotPasswordResetEmail;

class SendEmailVerification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

     protected $email;
     protected $email_token;
     protected $contact_name;
     protected $task;



     /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $email_token, $contact_name, $task)
    {
        $this->email = $email;
        $this->email_token = $email_token;
        $this->contact_name = $contact_name;
        $this->task = $task;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try 
        {
            if($this->task == "reset_password")
            {
                $emailto = new SendPasswordResetEmail($this->email_token,$this->contact_name);
                Mail::to($this->email)->send($emailto);
                // echo ("send email for rest password successful");

            }  
            else if($this->task == "verify_email"){
                $emailto = new EmailVerification($this->email_token,$this->contact_name);
                Mail::to($this->email)->send($emailto);  
                // echo ("send email for email verification successful");          
      
            }
            else if($this->task == "forgot_password"){
                $emailto = new SendForgotPasswordResetEmail($this->email_token,$this->contact_name);
                //   echo ("send email for email verification successful");   
                //   echo($this->email_token); 
                Mail::to($this->email)->send($emailto);  
                // echo ("send email for email verification successful");          
      
            }
        } catch (\Exception $e) 
        {
             $Exception_Error_Message = $e->getMessage();
            \Log::info("Could Not Execute Send Email Verification Job! Exception Message: $Exception_Error_Message");   
        }
       
    }
}

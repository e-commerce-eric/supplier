<?php

namespace App\Jobs\V1\Request\Supplier;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Mail\EmailApproval;

class ApproveEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

     protected $email_token;


     /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email_token)
    {
        $this->email_token = $email_token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try 
        {
          $emailto = new EmailApproval($this->email_token);
         Mail::to("sgachari@dibon.co.ke")->send($emailto);
       
        } catch (\Exception $e) 
        {
             $Exception_Error_Message = $e->getMessage();
            \Log::info("Could Not Execute Send Email Approval Job! Exception Message: $Exception_Error_Message");   
        }
       
    }
}

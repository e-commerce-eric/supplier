<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPasswordResetEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $email_token;
    protected $name;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_token, $name)
    {
        $this->email_token = $email_token;
        $this->name = $name;    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {
        return $this->view('emails.reset-password-email')->with([
            'email_token' => $this->email_token,
            'name' => $this->name
]);
    }
}

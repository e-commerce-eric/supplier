<?php


namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Validator;
use Log, Exception, DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use JWTAuth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    public function getAllCategorys()
    {
      try{
            $categorys = Category::select(
               'category_name',
               'id',
               'description',
               'picture', 
               'status',
            )->where('status','=',1)
            ->take(20000)
            ->get();
            return response()->json(compact('categorys'));
          }
          catch (Exception $e) { 
              Log::error('Exception ' . $e);     
              DB::rollback();
              return response()->json(['An error Occured,please try again later'], 400);               
          }
    }
    public function createCategory(Request $request)
    {
      try{
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $validator = Validator::make($request->all(), [
            'picture'=>'required|file|image|mimes:jpeg,png,gif,webp|max:2048',
            'category_name' => 'required|string|max:11',
            'description' => 'required|string|max:255',
            'status' => 'required|string|max:255',  
        ]);
        $result = "Category could not be created";
       
            if($request->hasfile('picture'))
            {
              //  foreach($request->file('picture') as $photo)
              //  {
                if (!$request->file('picture')->isValid()) {
                    throw new \Exception('Error on upload file: '.$file->getErrorMessage());
                }
                   $name = time().'.'.$request->file('picture')->extension();
                   $request->file('picture')->move(public_path().'/files/', $name);  

                   $category= new Category();
                   $category->picture="http://127.0.0.1:8000/public/files/".$name;
                   $category->category_name = $request->category_name;
                   $category->description = $request->description;
                   $category->status = $request->status;
                   $category->last_edited_by = $user->id;
                   $category->reference_id =  'category'.str_random(10);
                   $category->save();

            $result= "Category created Successfully";

              //  }
            }

        return response()->json(compact('result'),200);
      }
      catch (Exception $e) { 
          Log::error('Exception ' . $e);     
          DB::rollback();
          return response()->json(['An error Occured,please try again later'], 400);               
      }
    }
    public function getCategory($id)
    {
      try{
        if (Category::where('id', $id)->exists()) {
            $category = Category::where('id', $id)->get();
            return response()->json(compact('category'));
          } else {
            return response()->json([
              "message" => "Category not found"
            ], 404);
          }  
        }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
        }     
    }

    public function updateCategory(Request $request, $id)
    {
      try{
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $name = '';

        if (Category::where('id', $id)->exists()) {
            if($request->hasfile('photos'))
            {
                $result= "image upload failed";
    
                if (!$photo->isValid()) {
                    throw new \Exception('Error on upload file: '.$file->getErrorMessage());
                }
             
                   $name = time().'.'.$request->picture->extension();
                   $request->picture->move(public_path().'/files/', $name);  
            }
            
            $category = Category::find($id);
            $category->category_name = is_null($request->category_name) ? $category->category_name : $request->category_name;
            $category->description = is_null($request->description) ? $category->description : $request->description;
            $category->status = is_null($request->status) ? $category->status : $request->status;
            $category->picture= is_null($request->picture) ? $category->picture :"http://127.0.0.1:8000/public/files/".$name;
            $category->last_edited_by = $user->id;
            $category->save();
    
            return response()->json([
                "message" => "record updated successfully"
            ], 200);
            } else {
            return response()->json([
                "message" => "Category not found"
            ], 404);
        }
        return response()->json(compact('result'), 200);   
      }
      catch (Exception $e) { 
          Log::error('Exception ' . $e);     
          DB::rollback();
          return response()->json(['An error Occured,please try again later'], 400);               
      }    
    }

    public function deleteCategory ($id) {
      try{
        if(Category::where('id', $id)->exists()) {
            $category = Category::find($id);
            $category->delete();
    
            return response()->json([
              "message" => "record deleted successfully"
            ], 202);
          } else {
            return response()->json([
              "message" => "Category not found"
            ], 404);
          }
        }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
        }     
       }
    
}

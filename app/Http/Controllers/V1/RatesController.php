<?php


namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rate;
use Illuminate\Support\Facades\Validator;
use Log;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use JWTAuth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class RatesController extends Controller
{
    public function getAllRatess()
    {
            $ratess = Rate::select(
               'category_id',
               'profit',
               'type',
               'id'
            )->get();
            return response()->json(compact('ratess'));
            }
    public function createRates(Request $request)
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|string|max:11',
            'profit' => 'required|string|max:255',
            'type' => 'required|string|max:255',  
        ]);
        $result = "Rates could not be created";
       
                   $rates= new Rates();
                   $rates->category_id = $request->category_id;
                   $rates->profit = $request->profit;
                   $rates->type = $request->type;
                   $rates->last_edited_by = $user->id;
                   $rates->reference_id =  'rates'.str_random(10);
                   $rates->save();

            $result= "Rates created Successfully";

           
        return response()->json(compact('result'));
    }
    public function getRates($id)
    {
        if (Rate::where('id', $id)->exists()) {
            $rates = Rate::where('id', $id)->get();
            return response()->json(compact('rates'));
          } else {
            return response()->json([
              "message" => "Rates not found"
            ], 404);
          }       
    }

    public function updateRates(Request $request, $id)
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $name = '';

        if (Rate::where('id', $id)->exists()) {
           
            
            $rates = Rate::find($id);
            $rates->category_id = is_null($request->category_id) ? $rates->category_id : $request->category_id;
            $rates->profit = is_null($request->profit) ? $rates->profit : $request->profit;
            $rates->type = is_null($request->type) ? $rates->type : $request->type;
            $rates->last_edited_by = $user->id;
            $rates->save();
    
            return response()->json([
                "message" => "record updated successfully"
            ], 200);
            } else {
            return response()->json([
                "message" => "Rates not found"
            ], 404);
        }
        return response()->json(compact('result'));       
    }

    public function deleteRates ($id) {
        if(Rate::where('id', $id)->exists()) {
            $rates = Rate::find($id);
            $rates->delete();
    
            return response()->json([
              "message" => "record deleted successfully"
            ], 202);
          } else {
            return response()->json([
              "message" => "Rates not found"
            ], 404);
          }
        // logic to delete a rates record goes here
      }
    
}

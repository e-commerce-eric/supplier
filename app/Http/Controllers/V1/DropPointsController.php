<?php


namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Droppoint;
use Illuminate\Support\Facades\Validator;
use Log;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use JWTAuth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DropPointsController extends Controller
{
    public function getAllDroppoints()
    {
            $drop_points = Droppoint::select(
               'location',
               'price',
               'status',
               'id'
            )->where('status','=',1)
            ->take(20000)
            ->get();
            return response()->json(compact('drop_points'));
            }
    public function createDroppoint(Request $request)
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $validator = Validator::make($request->all(), [
            'location' => 'required|string|max:11',
            'price' => 'required|string|max:255',
            'status' => 'required|string|max:255',  
        ]);
        $result = "Droppoint could not be created";
       
                   $drop_point= new Droppoint();
                   $drop_point->location = $request->location;
                   $drop_point->price = $request->price;
                   $drop_point->status = $request->status;
                   $drop_point->last_edited_by = $user->id;
                   $drop_point->reference_id =  'drop_point'.str_random(10);
                   $drop_point->save();
            $result= "Droppoint created Successfully";
            
        return response()->json(compact('result'));
    }
    public function getDroppoint($id)
    {
        if (Droppoint::where('id', $id)->exists()) {
            $drop_point = Droppoint::where('id', $id)->get();
            return response()->json(compact('drop_point'));
          } else {
            return response()->json([
              "message" => "Droppoint not found"
            ], 404);
          }       
    }

    public function updateDroppoint(Request $request, $id)
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $name = '';

        if (Droppoint::where('id', $id)->exists()) {
           
            
            $drop_point = Droppoint::find($id);
            $drop_point->location = is_null($request->location) ? $drop_point->location : $request->location;
            $drop_point->price = is_null($request->price) ? $drop_point->price : $request->price;
            $drop_point->status = is_null($request->status) ? $drop_point->status : $request->status;
            $drop_point->last_edited_by = $user->id;
            $drop_point->save();
    
            return response()->json([
                "message" => "record updated successfully"
            ], 200);
            } else {
            return response()->json([
                "message" => "Droppoint not found"
            ], 404);
        }
        return response()->json(compact('result'));       
    }

    public function deleteDroppoint ($id) {
        if(Droppoint::where('id', $id)->exists()) {
            $drop_point = Droppoint::find($id);
            $drop_point->delete();
    
            return response()->json([
              "message" => "record deleted successfully"
            ], 202);
          } else {
            return response()->json([
              "message" => "Droppoint not found"
            ], 404);
          }
        // logic to delete a drop_point record goes here
      }
    
}

<?php
namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth, Exception, Log, Queue, DB;
use App\Models\OrderDetails;
use App\Models\OrderSettlements;

class SupplierReportsController extends Controller
{
    public function salesReport()
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
          try {
              $sales = OrderDetails::select(
                  "order_details.delivery_date",
                  "order_details.order_number",
                  "order_details.total",
                  "order_details.discount",
                  "orders.paid",
                  "orders.ship_date",
                  "orders.transaction_status",
                  "products.product_name"
              )
              ->leftJoin('orders', 'order_details.order_id', '=', 'orders.id')
              ->leftJoin('products', 'order_details.product_id', '=', 'products.id')
              ->where("products.supplier_id","=", $user->id)
              ->orderBy("order_details.id","ASC")
              ->get();
              return response()->json(compact('sales'), 200);

           }
          catch (Exception $e) { 
              Log::error('Exception ' . $e);     
              DB::rollback();
              return response()->json(['An error Occured,please try again later'], 400);               
          }
    }
    public function deliveryReport()
    {
         if (!$user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
      }
        try {
            $delivery_report = OrderDetails::select(
                "orders.reference_id",
                "order_details.delivery_date",
                "order_details.dispatch_date",
                "orders.transaction_status",
                "shippers.rider_name",
                "products.product_name"
            )
            ->leftJoin('orders', 'order_details.order_id', '=', 'orders.id')
            ->leftJoin('products', 'order_details.product_id', '=', 'products.id')
            ->leftJoin('shippers', 'orders.shipper_id', '=', 'shippers.id')
            ->where("products.supplier_id","=", $user->id)
            ->orderBy("order_details.id","ASC")
            ->get();
            return response()->json(compact('delivery_report'), 200);            
            //Order number, rider name, dispatch date & time, delivery status, delivery date & time
         }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
        }
    }
    public function settlementReport()
    {
        
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
            $settlement_report = OrderSettlements::select(
                "order_settlements.payment_reference_number",
                "order_settlements.settlement_date",
                "orders.reference_id",
                "order_settlements.amount"
            )
            ->leftJoin('orders', 'order_settlements.order_id', '=', 'orders.id')
            ->leftJoin('suppliers', 'order_settlements.supplier_id', '=', 'suppliers.id')
            ->where("order_settlements.supplier_id","=", $user->id)
            ->orderBy("order_settlements.id","ASC")
            ->get();
            return response()->json(compact('settlement_report'), 200); 
            //Settlement Date & Time, Order number, Amount
         }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
        }
    }
    public function returnedItemsReport()
    {
         if (!$user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
      }
        try {
         }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
        }
    }
}

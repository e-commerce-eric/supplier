<?php


namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\Image;
use App\Models\Orders;
use App\Models\Rate;
use Illuminate\Support\Facades\Validator;
use Log,Exception, DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use JWTAuth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
// use App\Http\Requests\UpdateProductRequest;


class ProductsController extends Controller
{
 
  public function index()
  {
    try {
          return Product::select(
              'products.supplier_id',//
              'products.id AS id',//
              'products.product_name',
              'products.category_id',
              'products.product_description',
              'products.quantity_per_unit',
              'products.unit_price',
              'products.available_size',
              'products.available_color',
              'products.available',
              'products.discount',
              'products.unit_weight',
              'products.units_on_order',
              'products.units_on_stock',
              'products.current_order',
              'products.note',
              'products.product_reference_number',
              'products.total_product_price',
              'products.previous_price',
              'products.days_required_to_prepare',
              'products.dates',
              'products.approved',
              'products.capacity_for_service',
              'categorys.category_name'
          )
          ->leftJoin('categorys', 'products.category_id', '=', 'categorys.id')

          ->where('available','=',1);
          // ->where('approved','=',1);
        }
        catch (Exception $e) { 
             Log::error('Exception ' . $e);     
             DB::rollback();
             return response()->json(['An error Occured,please try again later'], 400);               
        }
    }
    public function getAllProducts(Product $image)
    {
      try {
          // $all_products = $this->index()->where('approved','=',1)->get();
                 $all_products = $this->index()->where('approved','=',1)->get()
                 ->map(function($details){
                  $colors =$details->available_color;
                  $available_color = json_decode($colors);
                  $concat_colors = array();
                  foreach($available_color as $key => $colors)
                  {
                    $concat_colors[] = $colors;
                  }
                  $available_color = join(",", $concat_colors);
                  $details->available_color = $available_color;

                  return $details;
                 });
                //  echo($all_products); die();
            $products = $all_products->load('firstImage');
            return response()->json(compact('products'), 200);
          }
          catch (Exception $e) { 
               Log::error('Exception ' . $e);     
               DB::rollback();
               return response()->json(['An error Occured,please try again later'], 400);               
          }
      }
      public function getProductsForOneSupplier(Request $request)
      
      {
      //  print_r($request->all());
        // print_r(header('Authorization'));
        if (!$user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
      }
          
        try {
              $all_products = $this->index()
                  ->where('supplier_id','=',$user->id)
                  ->orderBy('products.id','ASC')
                  ->get()->map(function($details){
                    $colors =$details->available_color;
                    $available_color = json_decode($colors);
                    $concat_colors = array();
                    foreach($available_color as $key => $colors)
                    {
                      $concat_colors[] = $colors;
                    }
                    $available_color = join(",", $concat_colors);
                    $details->available_color = $available_color;
  
                    return $details;
                   });
              $products = $all_products->load('Image');
              return response()->json(compact('products'), 200);
            }
            catch (Exception $e) { 
                 Log::error('Exception ' . $e);     
                 DB::rollback();
                 return response()->json(['An error Occured,please try again later'], 400);               
            }
        }
      
    public function createProduct(Request $request)
    {
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }

        try {
          $validator = Validator::make($request->all(), [
            'photos' => 'required|array',
            'photos.*' => 'image|mimes:jpg,jpeg,png,gif,JPG, JPEG, PNG, GIF|max:5048',
            'category_id' => 'required|string|max:11',
            'product_name' => 'required|string|max:255',
            'unit_price' => 'required|string|max:255',
            'unit_price' => 'required|numeric|between:0,999999999', 
            'category_id' => 'required|string|max:255',
            'product_description' => 'required|string|max:2000',
            'quantity_per_unit' => 'string|max:255',  

        ]);
        $result = "";        
        if($validator->fails()){
          return response()->json($validator->errors(), 400);
      }
      $category_size = Category::where('need_size',"=",1)->pluck('id');
      if(in_array($request->category_id, json_decode($category_size))){
        $size_validator = Validator::make($request->all(), [         
          'available_size' => 'required|max:1000',
      ]);
      if($size_validator->fails()){
        return response()->json($size_validator->errors(), 400);
      }
    
    }

    if(!empty($request->dates)){
      $dates_validator = Validator::make($request->all(), [         
        'days_required_to_prepare' => 'required|numeric|between:0,90',
    ]);
    if($dates_validator->fails()){
      return response()->json($dates_validator->errors(), 400);
    }
  
  }
    $category_color = Category::where('need_color',"=",1)->pluck('id');
      if(in_array($request->category_id, json_decode($category_color))){
        $color_validator = Validator::make($request->all(), [         
          'available_color' => 'required|max:1000',
      ]);
      if($color_validator->fails()){
        return response()->json($color_validator->errors(), 400);
}
      }
     
     
        $product = Product::create([
          
            'supplier_id' => $user->id,
            'previous_price' => $request->previous_price,
            'days_required_to_prepare' => $request->days_required_to_prepare,
            'dates' => $request->dates,//checkbox
            'capacity_for_service' => $request->capacity_for_service,
            'category_id' => $request->category_id,
            'product_name' => $request->product_name,
            'product_description' => $request->product_description,
            'quantity_per_unit' => $request->quantity_per_unit,
            'unit_price' => $request->unit_price,//current price
            'available' => $request->available !=''? $request->available: 1,
            'available_size' => $request->available_size, //passed as {"size:number_of_that_size_available"}
            'available_color' => $request->available_color,//passed as {"color:number_of_that_color_available"}
            'discount' => $request->discount,//flat fee
            'unit_weight' => $request->unit_weight,
            'units_on_order' => $request->units_on_order,//2
            'units_on_stock' => $request->units_on_stock, //quantity_available_in_stock//10 -1, 4-2
            'current_order' => $request->current_order,                   
            'product_reference_number' => $request->product_reference_number,
            'last_edited_by' => $user->id,
            'reference_id' => str_random(10)
            ]);
            $rate = Rate::select('profit','type','VAT')->where('category_id', '=', $product->category_id)->get();
            $gross_price =(json_decode($rate)[0]->type)==0 ?(((json_decode($rate)[0]->profit) *  $product->unit_price) / 100) + $product->unit_price +  $product->discount:(json_decode($rate)[0]->profit) + $product->unit_price +  $product->discount;//0 for percentage 1 for flat fee

            $total_product_price = $gross_price + ($gross_price * (json_decode($rate)[0]->VAT/100));
 
            Product::where('id','=',$product->id)
            ->update(['total_product_price'=>$total_product_price]);
            if($request->hasfile('photos'))
            {

               foreach($request->file('photos') as $photo)
               {
                if (!$photo->isValid()) {
                    throw new \Exception('Error on upload file: '.$file->getErrorMessage());
                }
            

                   $name = str_random(5).time().'.'.$photo->extension();
                   $photo->move(public_path().'/files/', $name);  

                   $file= new Image();
                   $file->file_path="http://127.0.0.1:8000/files/".$name;
                   $file->product_id = $product->id;
                   $file->last_edited_by = $user->id;
                   $file->reference_id = str_random(10);
                   $file->save();

               }
            }

        return response()->json(compact('result'), 200);
      }
      catch (Exception $e) { 
           Log::error('Exception ' . $e);     
           DB::rollback();
           return response()->json(['An error Occured,please try again later'], 400);               
      }
    }
   
    public function getProduct(Product $id)
    {
      try{
          $product = $id->load('image');
            return response()->json(compact('product'), 200);
          }
        catch (Exception $e) { 
             Log::error('Exception ' . $e);     
             DB::rollback();
             return response()->json(['An error Occured,please try again later'], 400);               
        }   
    }

    public function updateProduct(Request $request,$id)
    {
      // Log::info("Request",["we hit it"]);
      // Log::info("Request",[$request->all()]); //die();

      try{
        if (!$user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['user_not_found'], 404);
        }
        $validator = Validator::make($request->all(), [
          'photos' => 'array',
          'photos.*' => 'image|mimes:jpg,jpeg,png,gif,JPG, JPEG, PNG, GIF|max:5048',
          'category_id' => 'required|string|max:11',
          'product_name' => 'required|string|max:255',
          'unit_price' => 'required|string|max:255',
          'unit_price' => 'required|numeric|between:0,999999999', 
          'category_id' => 'required|string|max:255',
          'product_description' => 'required|string|max:2000',
          // 'previous_price'=> 'numeric',
          // 'quantity_per_unit' => 'string|max:255',  
          // 'capacity_for_service' => 'numeric',
          // "available_size"=>"available_size",
          // "days_required_to_prepare" => "numeric",
          // "available_size"=>"available_size",
          // "dates" => "numeric",
          // "available_size"=>"available_size",
          // "units_on_order" =>"numeric",
          // "units_on_stock" => "numeric"
      ]);
      $result = "";        
      if($validator->fails()){
        return response()->json($validator->errors(), 400);
    }

        Product::where('id', $id)->update($request->only('previous_price', 'days_required_to_prepare', 'dates', 'capacity_for_service', 'category_id', 'product_name', 'product_description', 'quantity_per_unit', 'unit_price', 'available', 'available_size', 'available_color', 'discount', 'unit_weight', 'units_on_order', 'units_on_stock', 'current_order', 'product_reference_number'));
      
        Product::where('id','=',$id)
        ->update(['approved'=>0]);
        $product = Product::where('id','=',$id)->get();

        $rate = Rate::select('profit','type','VAT')->where('category_id', '=', $product[0]->category_id)->get();
        $gross_price =(json_decode($rate)[0]->type)==0 ?(((json_decode($rate)[0]->profit) *  $product[0]->unit_price) / 100) + $product[0]->unit_price +  $product[0]->discount:(json_decode($rate)[0]->profit) + $product[0]->unit_price +  $product[0]->discount;//0 for percentage 1 for flat fee

         $total_product_price = $gross_price + ($gross_price * (json_decode($rate)[0]->VAT/100));

        Product::where('id','=',$product[0]->id)
        ->update(['total_product_price'=>$total_product_price, 'last_edited_by' => $user->id,
        ]);
        $result= "Updated Successfully";

        if($request->hasfile('photos'))
        {
            $result= "image upload failed";
            Log::info("allimages",["image iko"]);

           foreach($request->file('photos') as $photo)
           {

            if (!$photo->isValid()) {
                throw new \Exception('Error on upload file: '.$file->getErrorMessage());
            }
           
               $name = str_random(5).time().'.'.$photo->extension();
               $photo->move(public_path().'/files/', $name);  
               

            $allimages = Image::where('product_id', '=', $id)->get();

            $image_count=$allimages->count();

             if ($image_count >0) {
                Image::updateOrCreate(["product_id" => $id],
                ["last_edited_by" =>$user->id,
                 "file_path" => "http://127.0.0.1:8000/files/".$name]);
                $result= "Uploaded Successfully";
                Log::info("image",[$id."http://127.0.0.1:8000/files/".$name]);
             }
            
           }
        }
        
        return response()->json(compact('result'), 200);
      }
      catch (Exception $e) { 
           Log::error('Exception ' . $e);     
           DB::rollback();
           return response()->json(['An error Occured,please try again later'], 400);               
      }
       
    }
  
    public function deleteProduct ($id) {
      try{
        if (!$user = JWTAuth::parseToken()->authenticate()) {
          return response()->json(['user_not_found'], 404);
      }
        if(Product::where('id', $id)->exists() && Orders::where('product_id','!=', $id)) {
            $product = Product::find($id);
            
            $product->delete();
            Log::info("Product deleted",[$product."deleted by".$user->id]);

            Product::where('id','=',$id)
                    ->update(['last_edited_by' => $user->id,
                    ]);
    
            return response()->json([
              "message" => "records deleted successfully"
            ], 200);
          } else {
            return response()->json([
              "message" => "Product not found"
            ], 404);
          }
        }
        catch (Exception $e) { 
             Log::error('Exception ' . $e);     
             DB::rollback();
             return response()->json(['An error Occured,please try again later'], 400);               
        }
      }
    
}

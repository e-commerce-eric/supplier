<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Carbon\Carbon;    
use JWTAuth, Exception, Log, Queue, DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Jobs\SendEmailVerification;
use App\Jobs\SendPasswordResetLinkSeller;
use App\Jobs\V1\Request\Supplier\ApproveEmails;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;

class SuppliersController extends Controller
{  
    public $loginAfterSignUp = true;
    public function authenticate(Request $request)
    {
        try{
                $activated=Supplier::where('email',$request->email)->value('activated');
                $approved=Supplier::where('email',$request->email)->value('approved');
            
            if($activated == 1 && $approved==1){
                    $credentials = $request->only('email', 'password');
                }else if($activated == ''){
                    $error="The Email address does not exist.";
                    return response()->json(compact('error'));
                }
                else if($approved == ''){
                    $error="Account pending approval.";
                    return response()->json(compact('error'));
                }
                else{
                    $error="You account is not activated";
                    return response()->json(compact('error'));
                }
                try {
                    if (! $token = JWTAuth::attempt($credentials)) {
                        $error="invalid credentials";
                        return response()->json(compact('error'));
                    }
                } catch (JWTException $e) {
                    return response()->json(['error' => 'could_not_create_token'], 500);
                }

                $supplier_id=Supplier::where('email',$request->email)->value('id');
                $name=Supplier::where('email',$request->email)->value('company_name');
        //$supplier=Products::where('id',$supplier_id)->take(1000)->get();
                return response()->json(compact('name'), 200);
            }
            catch (Exception $e) { 
                 Log::error('Exception ' . $e);     
                 DB::rollback();
                 return response()->json(['An error Occured,please try again later'], 400);               
            }
    }
    public function register(Request $request)
    {
        try{
        $validator = Validator::make($request->all(), [
            'company_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',   
            'last_name' => 'required|string|max:255',   
            'phone' => 'required|string|max:255',         
            'address_1' => 'required|string|max:255',
            'kra' => 'required|mimes:pdf|max:2048',
            'registration_cert' => 'required|mimes:pdf|max:2048',
            'license' => 'required|mimes:pdf|max:2048',
            // 'type_of_goods_or_service' => 'required|string|max:255',
            'user_type' => 'required|numeric',   
            'email' => 'required|string|email|max:255|unique:suppliers',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if($validator->fails()){
                      return response()->json($validator->errors(), 400);
        }
      //upload files to storage
  
        $krafileName = 'kra'.time().'.'.$request->kra->extension();  
   
        $request->kra->move(public_path().'/files/', $krafileName);

        Log::info("kra file", [$request->kra]);

        $registration_certfileName = 'registration_cert'.time().'.'.$request->registration_cert->extension();  
   
        $request->registration_cert->move(public_path().'/files/', $registration_certfileName);

        Log::info("registration_cert file", [$request->registration_cert]);

        $licensefileName = 'license'.time().'.'.$request->license->extension();  
   
        $request->license->move(public_path().'/files/', $licensefileName);

        Log::info("license file", [$request->license]);

        $supplier = Supplier::create([
        'company_name' => $request->company_name,  
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'address_1' => $request->address_1,
        'address_2' => $request->address_2,
        'town_or_city' => $request->town_or_city,
        'county' => $request->county,
        'postalcode' => $request->postalcode,
        'country' => $request->country,
        'phone' => $request->phone,
        'phone' => $request->user_type,
        'url' => $request->url,
        'type_of_goods_or_service' => $request->type_of_goods_or_service,
        'kra' => "http://127.0.0.1:8000/files/". $krafileName,
        'registration_cert' =>  "http://127.0.0.1:8000/files/".$registration_certfileName,
        'license' =>  "http://127.0.0.1:8000/files/".$licensefileName,
        // 'other_documents' => $request->other_documents,
        'logo' => $request->logo,
        'payments_methods' => $request->payments_methods,
        'notes' => $request->notes,
        'email' => $request->email,
        'email_token' =>bcrypt($request->email.$request->ip()),
        'password' => bcrypt($request->password),
        'ip_address' => $request->ip(),
        'reference_id' =>str_random(10)     
      ]);
      $supplierUpdate= new Supplier();
                $supplierUpdate->last_edited_by = $supplier->id;                
                $supplierUpdate->update(); 
      $token = auth()->login($supplier);
      $task = "verify_email";

      dispatch(new SendEmailVerification( $request->email,$token, $request->first_name, $task));    


    return response()->json($this->respondWithToken($token), 200);

    }
    catch (Exception $e) { 
         Log::error('Exception ' . $e);     
         DB::rollback();
         return response()->json(['An error Occured,please try again later'], 400);               
    }
    }

    public function login(Request $request)
    {
        // if (! $token = $this->parseAuthHeader($header, $method)) { // all your get method not passed this step
        //     if (! $token = $this->request->query($query, false)) { // all your post method stucked here 
        //         throw new JWTException('The token could not be parsed from the request', 400);
        //     }
        //  }
        try{
            $credentials = $request->only(['email', 'password']);

            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }
            $activated=Supplier::where('email',$request->email)->value('activated');
            $approved=Supplier::where('email',$request->email)->value('approved');  
        
            try {
                if (! $token = JWTAuth::attempt($credentials)) {
                    $error="invalid credentials";
                    return response()->json(compact('error'), 401);
                }
                else if($activated == 1 && $approved==1){
                    $credentials = $request->only('email', 'password');
                }else if($activated == ''){
                    $error="You account is not activated.";
                    return response()->json(compact('error'), 401);
                }
                else if($approved == ''){
                    $error="Account pending approval.";
                    return response()->json(compact('error'), 401);
                }
                else{
                    $error="You account is not activated";
                    return response()->json(compact('error'), 401);
                }
  
            return response()->json($this->respondWithToken($token), 200);

            } catch (JWTException $e) {
                return response()->json(['error' => 'could_not_create_token'], 500);
            }

        }
        catch (Exception $e) { 
             Log::error('Exception ' . $e);     
             DB::rollback();
             return response()->json(['An error Occured,please try again later'], 400);               
        }
    }

    public function refresh()
    {
        try{
             return response()->json([$this->respondWithToken(auth()->refresh())], 200);
        }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
        }
    }
    
    public function getAuthSupplier(Request $request)
    {
        try{
            return response()->json(compact(auth()->supplier()), 200);
        }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
        }
    }


    public function getAuthenticatedSupplier()
    {    
        try{
            $user = '';    
            if( !empty(JWTAuth::getToken()))
            {
            try {
                if (!$user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
                }
            } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                    return response()->json(['token_expired'], $e->getStatusCode());

            } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                    return response()->json(['token_invalid'], $e->getStatusCode());

            }
                catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_absent'], $e->getStatusCode());
            }
        }
        $user_type= $user->user_type;
        return response()->json(compact('user','user_type'), 200);
            // return response()->json(compact('user'));
        } catch (JWTException $e) {
            return response()->json([
            'status' => 'error',
            'message' => 'An error occured, please try again.'
            ], 500);
        }
    }

    public function recover(Request $request)
    {
        try{
        $user = Supplier::where('email', $request->email)->first();
        if (!$user) {
            $error_message = "Your email address was not found.";
            return response()->json(['success' => false, 'error' => ['email'=> $error_message]], 401);
        }
        try {
            Password::sendResetLink($request->only('email'), function (Message $message) {
                $message->subject('Your Password Reset Link');
            });
        } catch (\Exception $e) {
            $error_message = $e->getMessage();
            return response()->json(['success' => false, 'error' => $error_message], 401);
        }
        return response()->json([
            'success' => true, 'data'=> ['message'=> 'A reset email has been sent! Please check your email.']
        ]);
        } catch (JWTException $e) {
            return response()->json([
            'status' => 'error',
            'message' => 'Failed to logout, please try again.'
            ], 500);
        }
    }

    public function logout(Request $request)
    { 
        // if (! $token = $this->parseAuthHeader($header, $method)) { // all your get method not passed this step
        //     if (! $token = $this->request->query($query, false)) { // all your post method stucked here 
        //         throw new JWTException('The token could not be parsed from the request', 400);
        //     }
        //  }       
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
            return response()->json([
                'status' => 'success',
                'message'=> "User successfully logged out."
            ]);
            return response()->json(['message'=>'Successfully logged out'], 200);
        } catch (JWTException $e) {
            return response()->json([
            'status' => 'error',
            'message' => 'Failed to logout, please try again.'
            ], 500);
        }
       
    }
    protected function respondWithToken($token)
    {
        try{
            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth('api')->factory()->getTTL() * 60
            ], 200);
            }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
        }
    }
    public function emailVerification($email_token){     
        try{           
            if (! $user = JWTAuth::parseToken()->authenticate()) {                    
                return response()->json(['user_not_found'], 404);
            }

            $activate_user = Supplier::where('email_token', '=', $email_token)->first();

            $created_at=json_decode($user)->created_at;
            $id=json_decode($user)->id;
            $supplier_id=json_decode($user)->id;
        
            $current = Carbon::now();
            LOG::info('Current time : '.$current." Created At : ".$created_at);
            LOG::info('Diff : '.$current->diffInHours($created_at));
            $diff_min = $current->diffInHours($created_at);
            if($diff_min < 2){
                $user=Supplier::find($id);
                $user->activated=1;
                $user->save();                
                // $result="success";
            }
            Queue::pushOn('email_aproval', new ApproveEmails($email_token));
            // return response()->json(compact('result'));
            return redirect ("http://localhost:3000");
            }
            catch (Exception $e) { 
                Log::error('Exception ' . $e);     
                DB::rollback();
                return response()->json(['An error Occured,please try again later'], 400);               
            }
    }
    
    Public function resetPassword(Request $request)
    {
        try{
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        $validator = Validator::make($request->all(), [            
            'email' => 'required|string|email|max:255|exists:suppliers',
        ]);
        if($validator->fails()){
                      return response()->json($validator->errors(), 400);
        }
        // $email_token = Supplier::where("email", '=', $request->email)->value('email_token');
        $activated = Supplier::where("email", '=', $request->email)->value('activated');
        $name = Supplier::where("email", '=', $request->email)->value('first_name');
        $task = "reset_password";
        $email_token = JWTAuth::getToken();

        if ($activated == 1)
        {
            dispatch(new SendEmailVerification( $request->email,$email_token, $name, $task));
            return response()->json("Password reset link has been sent to your email", 200);
        }  
        return response()->json("Email is not activated", 200);
    }
    catch (Exception $e) { 
         Log::error('Exception ' . $e);     
         DB::rollback();
         return response()->json(['An error Occured,please try again later'], 400);               
    }

    }
    public function update(Request $request)
    {
        try{
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6|max:40|confirmed',
            // 'email' => 'required|string|email|max:255|exists:suppliers'
        ]);
        if($validator->fails()){
                      return response()->json($validator->errors(), 400);
        }
        Supplier::where('id', $user->id)
          ->update(['password' => bcrypt($request->password)]);
        return response()->json("Password Updated successfully", 200);
    }
    catch (Exception $e) { 
         Log::error('Exception ' . $e);     
         DB::rollback();
         return response()->json(['An error Occured,please try again later'], 400);               
    }
    }
    Public function forgotPassword(Request $request)
    {
        try{
        $validator = Validator::make($request->all(), [            
            'email' => 'required|string|email|max:255|exists:suppliers',
        ]);
        if($validator->fails()){
                      return response()->json($validator->errors(), 400);
        }
        $email_token = Supplier::where("email", '=', $request->email)->value('email_token');
        $activated = Supplier::where("email", '=', $request->email)->value('activated');
        $name = Supplier::where("email", '=', $request->email)->value('first_name');
        $task = "forgot_password";

        if ($activated == 1)
        {
            dispatch(new SendEmailVerification( $request->email,$email_token, $name, $task));
            return response()->json("Password reset link has been sent to your email", 200);
        }  
        return response()->json("Email is not activated", 200);
    }
    catch (Exception $e) { 
         Log::error('Exception ' . $e);     
         DB::rollback();
         return response()->json(['An error Occured,please try again later'], 400);               
    }

    }
    public function forgotPasswordUpdate(Request $request)
    {
        if(!empty($email_token = $request->header('email_token'))){
             try{
           
                $email_token = $request->header('email_token');

            $validator = Validator::make($request->all(), [
                'password' => 'required|min:6|max:40|confirmed',
                'email' => 'required|min:6|max:40|exists:suppliers',
            ]);
            $email_token_validator =Supplier::where('email', $request->email)->value('email_token');

            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }
            else if($email_token == '' )
            {
                return response()->json("Email token is required", 400);
            }

            else if ($email_token_validator != $email_token)
            {
                return response()->json("invalid inputs", 400);

            }
            // echo("email_token ".$request->header('email_token')); die();

            Supplier::where('email_token', $email_token)
            ->update(['password' => bcrypt($request->password)]);
            return response()->json("Password Updated successfully", 200);
        }
        catch (Exception $e) { 
            Log::error('Exception ' . $e);     
            DB::rollback();
            return response()->json(['An error Occured,please try again later'], 400);               
       }
    }
   else{
    return response()->json(['An error Occured,please try again later'], 400);  
   }
    }
    

}
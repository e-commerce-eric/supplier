<?php
namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class UpdatePasswordController extends Controller
{


    public function verifyusers($token)
     {
       $check = User::where('email_token', '=', $token)
               ->where('activated', '=', 0)
               ->first();
        

         if ($check === null) 
           { 
             return redirect('/');
           
           }
         else
           {

           $user_id = $check->id;
           return view('auth.passwords.change-password', compact('user_id'));   
           
           }   

      }

    /**
     * Update or change the password for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6|max:40|confirmed',
            'user_id' => 'required',
        ]);

         //Change the password
       
        $user = User::find($request->user_id);
        $user->password = bcrypt($request->password);
        $user->activated = 1;
        $user->save(); 
        self::message('success', 'Password updated successfully');
    return redirect('/'); 

    }
}
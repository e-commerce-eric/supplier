<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    public function rules()
    {
        return [
            'category_id'         => [
                'required', 
                'exists:categorys'
            ],
        ];
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\AirtimeTransaction;
use App\Models\Supplier;
use Queue;
use App\Jobs\V1\Request\Suppier\ApproveEmails;

class ApproveEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'retry:emailapproval';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is a class to try transactions on models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $transactions = Supplier::where('status', 31)->take(100)->get();

        foreach ($transactions as $transaction) {
            
            Queue::pushOn('api', new ApproveEmails($transaction));

        }

        
    }
}
